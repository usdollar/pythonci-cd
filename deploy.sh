
echo 'deploy app to server Test: =======>'
rm -fr $HOME/pythonci-cd
git clone https://gitlab.com/usdollar/pythonci-cd.git
cd $HOME/pythonci-cd
sudo docker container stop pythonci-cd_web_1
sudo docker rmi -f registry.gitlab.com/usdollar/pythonci-cd
sudo docker-compose down
sudo docker-compose up -d
echo '=====> deploy success on Test server'
